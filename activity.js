// step 2
db.fruits.aggregate([
	{ $match: { "onSale": true } },
	{ $count: "fruitsOnSale" }
]);


// step 3
db.fruits.aggregate([
	{ $match: { stock: {$gt: 20} } },
	{ $count: "enoughStock" }
]);

// step 4 - Average
db.fruits.aggregate([
	{ $match: { "onSale": true } },
	{ $group: { "_id": "$supplier_id", "avg_price": { $avg: "$price" } } }
]);


// step 5 - Max
db.fruits.aggregate([
	{ $match: { "onSale": true } },
	{ $group: { "_id": "$supplier_id", "max_price": { $max: "$price" } } }
]);


// step 6 - Min
db.fruits.aggregate([
	{ $match: { "onSale": true } },
	{ $group: { "_id": "$supplier_id", "minprice": { $min: "$price" } } }
]);
